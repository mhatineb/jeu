

/**
 * INTERFACE
 */
import { Card } from "./entities";

/**
 * import images
 */

import ordinateur0 from "../public/CSS/img/ordinateur0.png"
import ordinateur1 from "../public/CSS/img/ordinateur1.png"
import ordinateur2 from "../public/CSS/img/ordinateur2.png"
import ordinateur3 from "../public/CSS/img/ordinateur3.png"
import ordinateur4 from "../public/CSS/img/ordinateur4.png"
import ordinateur5 from "../public/CSS/img/ordinateur5.png"
import cartememo from "../public/CSS/img/carte-memoire.png"

/**
 * Tableau d'objet pour chaque carte
 */
const myCard: Card[] = [
	{ index: 0, imgRecto: ordinateur0, isFlipped: false },
	{ index: 1, imgRecto: ordinateur1, isFlipped: false },
	{ index: 2, imgRecto: ordinateur2, isFlipped: false },
	{ index: 3, imgRecto: ordinateur3, isFlipped: false },
	{ index: 4, imgRecto: ordinateur4, isFlipped: false },
	{ index: 5, imgRecto: ordinateur5, isFlipped: false },
	{ index: 0, imgRecto: ordinateur0, isFlipped: false },
	{ index: 1, imgRecto: ordinateur1, isFlipped: false },
	{ index: 2, imgRecto: ordinateur2, isFlipped: false },
	{ index: 3, imgRecto: ordinateur3, isFlipped: false },
	{ index: 4, imgRecto: ordinateur4, isFlipped: false },
	{ index: 5, imgRecto: ordinateur5, isFlipped: false }
]

//Variables pour comparer mes cartes
let cardA: Card | null = null
let cardB: Card | null = null

//Variables pour rester dans les div de mes cartes
let hideCardA: HTMLDivElement
let visibleCardA: HTMLDivElement

//Variable si une carte est retournée ou non
let clickable = true

/**
 * Variable pour selectionner ma div container-fluid 
 */
const contain = document.querySelector<HTMLDivElement>('.container-fluid');

/**
 *  Variable pour créer mes éléments en dehors des cards
 */
const divRow = document.createElement('div');
divRow.classList.add('row');
contain?.append(divRow);

const divTitle = document.createElement('div');
divTitle.classList.add('divTitle');
divRow.append(divTitle);

const title = document.createElement('h1');
title.classList.add('text-center', 'title');
title.innerHTML = 'MEMORY GAME';
divTitle.append(title);

const divEnd = document.createElement('div');
divEnd.classList.add('text-center','end');
divEnd.innerHTML = 'BRAVO, tu es fantastique!!';
divRow.append(divEnd);

const divButton = document.createElement('div');
divButton.classList.add('divButton');
divRow.append(divButton);


const button = document.createElement('button');
button.classList.add('col-3', 'start');
button.innerHTML = 'Redémarrer une partie!';
divButton.append(button);


const allCards = document.querySelector<HTMLDivElement>("#allCards");
if (allCards)
divRow.append(allCards);


//Appelle de ma fonction pour mes cartes en aléatoire
randomCard()
/**
 * Boucle qui permet de générer mes cartes en Bootstrap et de les afficher
 */


addCard()
function addCard() {
	for (const card of myCard) {
	
		let divCol = document.createElement('div');
		divCol.classList.add('col-3', 'g-2', 'p-1', 'card');
		allCards?.append(divCol);
	
		let oneCard = document.createElement('div');
		oneCard.classList.add('oneCard');
		divCol.append(oneCard);
	
		let ordiCard = document.createElement('div');
		ordiCard.classList.add('ordi','hide');
		oneCard.append(ordiCard);
	
		let imgVisible = document.createElement('img');
		imgVisible.classList.add('imgVisible');
		ordiCard.append(imgVisible);
		imgVisible.src = card.imgRecto;
	
		let backCard = document.createElement('div');
		backCard.classList.add('back');
		oneCard.append(backCard);
	
		let imgHide = document.createElement('img');
		imgHide.classList.add('imgHide');
		backCard.append(imgHide);
		imgHide.src = cartememo;
	
		//J'appelle ma fonction qui compare mes cartes
	selectedCard(oneCard, backCard, ordiCard, card,);
}

	
    
    //Si je click sur le bouton, le jeu redémarre en aléatoire car j'appelle randomCard
    
		
}
    button.addEventListener(('click'), () => {
		if (button){
			
			location.reload()
			
			addCard()
		}
		
		
	});  

	// fonction pour afficher les cartes en aléatoire
	function randomCard() {
		myCard.sort(() => 0.5 - Math.random());
}


	// fonction pour signaler la fin du jeu au joueur
	function finish (myCard:Card[]) {
    
	let end = myCard.every(myCard => myCard.isFlipped);
	
	if(end) {
		if (allCards){
    allCards.style.display= 'none';
}	
	hideCardA.classList.toggle('hide');
	visibleCardA.classList.toggle('hide');
	divEnd.style.display = 'block';
	
	}
}

/**
 *  fonction qui vérifie si les cartes sont identiques 
 */
	function selectedCard(oneCard: HTMLDivElement,
	                  backCard: HTMLDivElement,
					  ordiCard: HTMLDivElement,
					  card: Card) {


	oneCard.addEventListener('click', () => {
        
		
		//Démarrage des cartes cachées
		if (card && clickable == true && card.isFlipped == false) {
			
			card.isFlipped = true;
			backCard.classList.toggle('hide');
			ordiCard.classList.toggle('hide');
			console.log(clickable);
            
			//Si la 1er variable est vide, on peut mettre l'image dedans
			if (cardA === null) {
				cardA = card;
				visibleCardA = ordiCard;
				hideCardA = backCard;
				
				
				//Si la 2ème carte cliquée est vide on met l'image dedans
			} else if (cardB === null) {
				cardB = card;
				clickable = false;
				

				//Comparaison des cartes
				if (cardA.index === cardB.index) {
					card.isFlipped = true;
					clickable = true;
					cardA = null;
					cardB = null;

                    finish(myCard);
					
				}
                 
				//Condition de cartes à retourner et que nous pouvons clicker  
				else {
					setTimeout(() => {
						if (cardA && cardB) {
							backCard.classList.toggle('hide');
							ordiCard.classList.toggle('hide');
							if (hideCardA)
								hideCardA.classList.toggle('hide');
							if (visibleCardA)
								visibleCardA.classList.toggle('hide');

							cardA.isFlipped = false;
							cardB.isFlipped = false;
							cardA = null;
							cardB = null;
							clickable = true;
						}
					}, 1000);					
				}				
			}			
		}   
	});
}














